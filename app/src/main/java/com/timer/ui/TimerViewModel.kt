package com.timer.ui.com.timer.ui

import androidx.lifecycle.ViewModel
import com.timer.ui.com.timer.domain.timer.Timer
import com.timer.ui.com.timer.ui.utils.TimestampMillisecondsFormatter
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map

class TimerViewModel(private val timer: Timer) : ViewModel() {

    private val _isTimerRunning = MutableStateFlow(false)
    val isTimerRunning: StateFlow<Boolean> = _isTimerRunning

    val time = timer.time.map {
        TimestampMillisecondsFormatter.format(it)
    }

    fun changeTimerState() {
        val currentValue = _isTimerRunning.value
        _isTimerRunning.value = !currentValue
        if (_isTimerRunning.value) {
            timer.start()
        } else {
            timer.reset()
        }
    }
}