package com.timer.ui.com.timer.ui.utils

import java.util.concurrent.TimeUnit

object TimestampMillisecondsFormatter {

    /**
     * @param timestamp in milliseconds
     * @return timestamp in the format "hh:mm:ss"
     */
    fun format(timestamp: Long): String {
        return String.format(
            "%02d:%02d:%02d",
            TimeUnit.MILLISECONDS.toHours(timestamp) % 60,
            TimeUnit.MILLISECONDS.toMinutes(timestamp) % 60,
            TimeUnit.MILLISECONDS.toSeconds(timestamp) % 60
        )
    }
}