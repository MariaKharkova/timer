package com.timer.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.timer.R
import com.timer.databinding.ActivityTimerBinding
import com.timer.ui.com.timer.ui.TimerViewModel
import com.timer.ui.com.timer.ui.utils.binding.viewBinding
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class TimerActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityTimerBinding::inflate)
    private val timerViewModel: TimerViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        initViews()
        initDataCollection()
    }

    private fun initViews() {
        binding.actionBtn.setOnClickListener { timerViewModel.changeTimerState() }
    }

    private fun initDataCollection() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    timerViewModel.isTimerRunning.collect { running ->
                        binding.actionBtn.text =
                            getString(if (running) R.string.timer_stop else R.string.timer_start)
                    }
                }
                launch {
                    timerViewModel.time.collect {
                        binding.timerCounter.text = it
                    }
                }
            }
        }
    }
}