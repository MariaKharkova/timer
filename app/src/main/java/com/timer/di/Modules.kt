package com.timer.ui.com.timer.di

import com.timer.ui.com.timer.domain.timer.Timer
import com.timer.ui.com.timer.domain.timer.TimerImpl
import com.timer.ui.com.timer.ui.TimerViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object Modules {

    private val timerModule = module {

        fun timer(): Timer = TimerImpl()

        single { timer() }
    }

    private val viewModelsModule = module {
        viewModel { TimerViewModel(get()) }
    }

    val modulesList = listOf(
        timerModule,
        viewModelsModule
    )
}