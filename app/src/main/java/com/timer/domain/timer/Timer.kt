package com.timer.ui.com.timer.domain.timer

import kotlinx.coroutines.flow.Flow

/**
 * Base timer interface
 */
interface Timer {
    val time: Flow<Long>
    fun start()
    fun reset()
}