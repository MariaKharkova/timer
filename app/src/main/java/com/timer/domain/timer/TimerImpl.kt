package com.timer.ui.com.timer.domain.timer

import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.isActive
import java.util.concurrent.TimeUnit

class TimerImpl(
    private val tick: Long = TimeUnit.SECONDS.toMillis(1),
    private val currentTime: () -> Long = { System.currentTimeMillis() }
) : Timer {
    private var elapsedTime: Long = 0L
    private val actions = MutableStateFlow(Action.Reset)

    override val time: Flow<Long> = actions.flatMapLatest { action ->
        when (action) {
            Action.Start -> {
                val time = flow {
                    val initial = currentTime() - elapsedTime
                    while (currentCoroutineContext().isActive) {
                        elapsedTime = currentTime() - initial
                        emit(elapsedTime)
                        delay(tick)
                    }
                }
                time.conflate()
            }
            Action.Reset -> {
                elapsedTime = 0L
                flowOf(elapsedTime)
            }
        }
    }

    override fun start() {
        actions.value = Action.Start
    }

    override fun reset() {
        actions.value = Action.Reset
    }

    private enum class Action {
        Reset,
        Start
    }
}