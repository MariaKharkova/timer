package com.timer.ui

import android.app.Application
import com.timer.ui.com.timer.di.Modules
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class TimerApp: Application() {

    override fun onCreate() {
        super.onCreate()
        // Start Koin DI
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@TimerApp)
            modules(Modules.modulesList)
        }
    }
}